/*
* cc1101.c
*
* Created: 26/02/2016 12:25:49
*  Author: Marius
*/


#include "cc1101.h"

#include <spi_master.h>
#include <ioport.h>
#include <stdbool.h>

void cc1101_init(void){
	static struct spi_device cc1101_dev;
	spi_master_init(SPI);
	cc1101_device = &cc1101_dev;
	spi_master_setup_device(SPI, cc1101_device, SPI_MODE_0, CC1101_BAUD_RATE, CC1101_SPI_CHIP_SEL);
	spi_enable(SPI);	
}

static inline uint8_t cc1101_wait_miso(void){
	
	ioport_enable_pin(PIN_PA23A_SPI_SCK);
	ioport_set_pin_dir(PIN_PA23A_SPI_SCK, IOPORT_DIR_INPUT);
	uint8_t timeout = CC1101_MISO_TIMEOUT;
	while(ioport_get_pin_level(PIN_PA21A_SPI_MISO)){
		timeout--;
		if(timeout==0)
			return CC1101_ERROR;
	}
	
	ioport_set_pin_mode(PIN_PA23A_SPI_SCK, MUX_PA23A_SPI_SCK);
	ioport_disable_pin(PIN_PA23A_SPI_SCK);

	return CC1101_OK;
}

static uint8_t spi_transceive_single(Spi *p_spi, uint8_t tx, uint8_t *rx){
	
	uint32_t timeout = SPI_TIMEOUT;

	timeout = SPI_TIMEOUT;
	while (!spi_is_tx_ready(p_spi)) {
		if (!timeout--) {
			return ERR_TIMEOUT;
		}
	}
	spi_write_single(p_spi, tx);
	
	timeout = SPI_TIMEOUT;
	while (!spi_is_rx_ready(p_spi)) {
		if (!timeout--) {
			return ERR_TIMEOUT;
		}
	}
	spi_read_single(p_spi, rx);

	return STATUS_OK;
}

static uint8_t spi_transceive_packet(Spi *p_spi, uint8_t *tx, uint8_t *rx, size_t len){
	
	uint32_t timeout = SPI_TIMEOUT;
	uint8_t val;
	uint32_t i = 0;

	while (len) {
		timeout = SPI_TIMEOUT;
		while (!spi_is_tx_ready(p_spi)) {
			if (!timeout--) {
				return ERR_TIMEOUT;
			}
		}
		spi_write_single(p_spi, tx[i]);

		timeout = SPI_TIMEOUT;
		while (!spi_is_rx_ready(p_spi)) {
			if (!timeout--) {
				return ERR_TIMEOUT;
			}
		}
		spi_read_single(p_spi, &val);

		rx[i] = val;
		i++;
		len--;
	}

	return STATUS_OK;
	
}

uint8_t cc1101_send_strobe(uint8_t strobe){
	
	uint8_t status = CC1101_ERROR;
	spi_select_device(SPI, cc1101_device);
//	cc1101_wait_miso();
	spi_transceive_single(SPI, strobe, &status);
	spi_deselect_device(SPI, cc1101_device);
	return status;
	
}

void cc1101_read_burst(uint8_t startAddr, uint8_t* data, uint8_t len){
	
	spi_select_device(SPI, cc1101_device);
//	cc1101_wait_miso();
	spi_write_single(SPI, cc1101_make_header(CC1101_HDR_READ, CC1101_HDR_BURST, startAddr));
	spi_read_packet(SPI, data, len);
	spi_deselect_device(SPI, cc1101_device);
		
}

void cc1101_write_burst(uint8_t startAddr, uint8_t* data, uint8_t len){
	
	spi_select_device(SPI, cc1101_device);
//	cc1101_wait_miso();
	spi_write_single(SPI, cc1101_make_header(CC1101_HDR_WRITE, CC1101_HDR_BURST, startAddr));
	spi_write_packet(SPI, data, len);
	spi_deselect_device(SPI, cc1101_device);
	
}

uint8_t cc1101_read_single_access(uint8_t addr){
	
	uint8_t data;
	spi_select_device(SPI, cc1101_device);
//	cc1101_wait_miso();
	spi_write_single(SPI, cc1101_make_header(CC1101_HDR_READ, CC1101_HDR_SA, addr));
	spi_transceive_single(SPI, 0, &data);
	spi_deselect_device(SPI, cc1101_device);
	return data;
	
}

void cc1101_write_single_access(uint8_t addr, uint8_t data){
	
		
	spi_select_device(SPI, cc1101_device);
	cc1101_wait_miso();
	spi_write_single(SPI, cc1101_make_header(CC1101_HDR_WRITE, CC1101_HDR_SA, addr));
	spi_write_single(SPI, data);
	spi_deselect_device(SPI, cc1101_device);
	
}
