/*
 * cc1101.h
 *
 * Created: 25/02/2016 13:48:32
 *  Author: Marius
 */ 


#ifndef CC1101_H_
#define CC1101_H_

#include <stdint.h>

#define CC1101_SPI_CHIP_SEL 0
#define CC1101_BAUD_RATE 5000000UL
#define CC1101_MISO_TIMEOUT 3
#define CC1101_MAX_PACKET_SIZE 256

#define CC1101_FIFO_SIZE 64
#define CC1101_FIFO_THR 7
#define CC1101_TX_THR ((15-CC1101_FIFO_THR)*4 + 1)
#define CC1101_RX_THR ((CC1101_FIFO_THR + 1)*4)

#define CC1101_HDR_READ 1
#define CC1101_HDR_WRITE 0
#define CC1101_HDR_BURST 1
#define CC1101_HDR_SA 0

#define CC1101_STATE_IDLE 0
#define CC1101_STATE_RX 1
#define CC1101_STATE_TX 2
#define CC1101_STATE_FSTXON 3
#define CC1101_STATE_CALIBRATE 4
#define CC1101_STATE_SETTLING 5
#define CC1101_STATE_RXFIFO_OVERFLOW 6
#define CC1101_STATE_TXFIFO_UNDERFLOW 7

#define CC1101_STATUS_MSK_CHIP_RDY 1<<7
#define CC1101_STATUS_MSK_STATE 7<<4
#define CC1101_STATUS_MSK_FIFO_BYTES 0x0F

#define cc1101_make_header(rw, burst, addr) ((rw<<7)|(burst<<6)|(addr))
#define cc1101_get_state(status) ((status & CC1101_STATUS_MSK_STATE)>>4)
#define cc1101_get_fifo_bytes(status) (status&CC1101_STATUS_MSK_FIFO_BYTES)

#define CC1101_ERROR 0xFF
#define CC1101_OK 0
#define CC1101_ERROR_CHIP_NRDY 1
#define CC1101_ERROR_SPI 2
#define CC1101_ERROR_STATE_CHANGED 3
#define CC1101_ERROR_FIFO_EMPTY 4
#define CC1101_ERROR_MISO_TIMEOUT 5
#define CC1101_WARNING_FIFO_UNDERFLOW 6

#define CC1101_RETURN_MSK_CODE 0x03
#define CC1101_RETURN_MSK_EXTRA 0xFC

#define cc1101_get_return_code(ret) (ret&CC1101_RETURN_MSK_CODE)
#define cc1101_get_return_len(ret) ((ret&CC1101_RETURN_MSK_EXTRA)>>2)
#define cc1101_make_return_value(code, xtra) ((xtra<<2)|code)

#define cc1101_make_strobe(rw, op) (cc1101_make_header(rw, CC1101_HDR_SA, op))

/* Device structure as defined by the spi service */

struct spi_device* cc1101_device;

void cc1101_init(void);

/*
The chips have 47 configuration registers (address 0 to address 0x2E). The R/W bit in the
address header controls if the register should be written or read, and the burst bit controls if it
is a single access or a burst access.
*/

// Configuration registers
#define CC1101_REG_CFG_IOCFG2 0x00
#define CC1101_REG_CFG_IOCFG1 0x01
#define CC1101_REG_CFG_IOCFG0 0x02
#define CC1101_REG_CFG_FIFOTHR 0x03
#define CC1101_REG_CFG_SYNC1 0x04
#define CC1101_REG_CFG_SYNC0 0x05
#define CC1101_REG_CFG_PKTLEN 0x06
#define CC1101_REG_CFG_PKTCTRL1 0x07
#define CC1101_REG_CFG_PKTCTRL0 0x08
#define CC1101_REG_CFG_ADDR 0x09
#define CC1101_REG_CFG_CHANNR 0x0A
#define CC1101_REG_CFG_FSCTRL1 0x0B
#define CC1101_REG_CFG_FSCTRL0 0x0C
#define CC1101_REG_CFG_FREQ2 0x0D
#define CC1101_REG_CFG_FREQ1 0x0E
#define CC1101_REG_CFG_FREQ0 0x0F
#define CC1101_REG_CFG_MODEMCFG4 0x10
#define CC1101_REG_CFG_MODEMCFG3 0x11
#define CC1101_REG_CFG_MODEMCFG2 0x12
#define CC1101_REG_CFG_MODEMCFG1 0x13
#define CC1101_REG_CFG_MODEMCFG0 0x14
#define CC1101_REG_CFG_DEVIATN 0x15
#define CC1101_REG_CFG_MCSM2 0x16
#define CC1101_REG_CFG_MCSM1 0x17
#define CC1101_REG_CFG_MCSM0 0x18
#define CC1101_REG_CFG_FOCCFG 0x19
#define CC1101_REG_CFG_BSCFG 0x1A
#define CC1101_REG_CFG_AGGCTRL2 0x1B
#define CC1101_REG_CFG_AGGCTRL1 0x1C
#define CC1101_REG_CFG_AGGCTR 0x1D
#define CC1101_REG_CFG_WOREVT1 0x1E
#define CC1101_REG_CFG_WOREVT0 0x1F
#define CC1101_REG_CFG_WORCTRL 0x20
#define CC1101_REG_CFG_FREND1 0x21
#define CC1101_REG_CFG_FREND0 0x22
#define CC1101_REG_CFG_FSCAL3 0x23
#define CC1101_REG_CFG_FSCAL2 0x24
#define CC1101_REG_CFG_FSCAL1 0x25
#define CC1101_REG_CFG_FSCAL0 0x26
#define CC1101_REG_CFG_RCCTRL1 0x27
#define CC1101_REG_CFG_RCCTRL0 0x28
#define CC1101_REG_CFG_FSTEST 0x29
#define CC1101_REG_CFG_PTEST 0x2A
#define CC1101_REG_CFG_AGCTEST 0x2B
#define CC1101_REG_CFG_TEST2 0x2C
#define CC1101_REG_CFG_TEST1 0x2D
#define CC1101_REG_CFG_TEST0 0x2E

// Status registers
#define CC1101_REG_STS_PARTNUM 0x30
#define CC1101_REG_STS_VERSION 0x31
#define CC1101_REG_STS_FREQEST 0x32
#define CC1101_REG_STS_LQI 0x33
#define CC1101_REG_STS_RSSI 0x34
#define CC1101_REG_STS_MARCSTATE 0x35
#define CC1101_REG_STS_WORTIME1 0x36
#define CC1101_REG_STS_WORTIME0 0x37
#define CC1101_REG_STS_PKTSTATUS 0x38
#define CC1101_REG_STS_VCO_VS_DAC 0x39
#define CC1101_REG_STS_TXBYTES 0x3A
#define CC1101_REG_STS_RXBYTES 0x3B
#define CC1101_REG_STS_RCCTRL1_STATUS 0x3C
#define CC1101_REG_STS_RCCTRL0_STATUS 0x3D


/*4.1 Single Access
For single access to the registers, the burst bit has to be set to 0. After transmitting the
address header, one can either transmit one data byte or read one byte, depending on the
R/W bit. After the data byte a new address is expected; hence, CSn can remain low.
*/
//////////////////////////////////////////////////////////////////////////

/**
 * \brief Writes a single byte in the CC1101 register.
 *
 * \param addr		Address of register to be written to.
 * \param data		Byte to be written in the register
 *
 */
void cc1101_write_single_access(uint8_t addr, uint8_t data);

/**
 * \brief Reads a single byte in the CC1101 register.
 *
 * \param addr		Address of register to be written to.
 *
 * \return			Read value
 */
uint8_t cc1101_read_single_access(uint8_t addr);

/*
4.2 Burst Access
When the burst bit is set, the radio expects one address byte and then consecutive data
bytes until terminating the access by setting CSn high.
*/
//////////////////////////////////////////////////////////////////////////


uint8_t cc1101_write_burst(uint8_t addr, uint8_t* data, uint8_t len);

uint8_t cc1101_read_burst(uint8_t addr, uint8_t* data, uint8_t len);


/*
4.3 Command Strobes
Command strobes are single byte instructions which will start an internal sequence (start RX,
enter power down mode etc). The command strobes share addresses with a set of status
registers (address 0x30 to address 0x3F). These status registers can not be accessed in
burst mode. If the burst bit is 1, a status register is accessed, if the burst bit is 0, a command
strobe is sent. A command strobe may be followed by any other SPI access without pulling
CSn high. After issuing an SRES command strobe the next command strobe can be issued
when the MISO pin goes low. The command strobes are executed immediately, with the
exception of the SPWD and the SXOFF strobes that are executed when CSn goes high.
*/

#define CC1101_STROBE_SRES 0x30
#define CC1101_STROBE_SFSTXON 0x31
#define CC1101_STROBE_SXOFF 0x32
#define CC1101_STROBE_SCAL 0x33
#define CC1101_STROBE_SRX 0x34
#define CC1101_STROBE_STX 0x35
#define CC1101_STROBE_SIDLE 0x36
#define CC1101_STROBE_SWOR 0x38
#define CC1101_STROBE_SPWD 0x39
#define CC1101_STROBE_SFRX 0x3A
#define CC1101_STROBE_SFTX 0x3B
#define CC1101_STROBE_SWORRST 0x3C
// Use SNOP (no operation) to get status byte
#define CC1101_STROBE_SNOP 0x3D

uint8_t cc1101_send_strobe(uint8_t strobe);

/*
5 Chip Status Byte
When the header byte, data byte or command strobe is sent on the SPI interface, the chip
status byte is sent from the radio on the MISO pin. The status byte contains key status
signals, useful for the MCU.
When writing to registers, the status byte is sent on the MISO pin each time a header byte or
data byte is transmitted on the MOSI pin. When reading from registers, the status byte is sent
on the MISO pin each time a header byte is transmitted on the MOSI pin. Be aware that the 4
LSB of the status byte (FIFO_BYTES_AVAILABLE) can give information on either the TX
FIFO (R/W = 0) or the RX FIFO (R/W = 1).
*/

#define CC1101_NR_REG_CFG 0x2E

#define CC1101_REG_FIFO 0x3F

#define CC1101_HEADER_READ_FIFO 0xFF
#define CC1101_HEADER_WRITE_FIFO 0x7F


#endif /* CC1101_H_ */