


#ifndef DDLS_H_
#define DDLS_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "./daedalus/services/nvm/ddls_nvm.h"

#define CC1101_GDO0_PIN 38
#define CC1101_GDO2_PIN 39

#define DDLS_INSTR_ACK 0
#define DDLS_INSTR_BEACON -1
#define DDLS_INSTR_JOIN_REQ -2
#define DDLS_INSTR_JOIN_ACK -3
#define DDLS_INSTR_JOIN_NACK -4
#define DDLS_INSTR_CONF_TIMING -5

#define DDLS_ADDR_COORD 0
#define DDLS_ADDR_BROADCAST 0xFF


typedef struct {
	uint16_t len;
	uint8_t addr_to;
	uint8_t addr_from;
	uint8_t* payload;
	uint8_t rssi;
	uint8_t lqi;
} DdlsPacket;


void ddls_init(void);

void ddls_read_packet(void);

void process_ddls(void);

#endif /* BASE_H_ */