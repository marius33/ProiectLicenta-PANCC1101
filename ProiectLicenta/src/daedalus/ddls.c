/*
 * ddls.c
 *
 * Created: 14/06/2016 20:53:03
 *  Author: Marius
 */ 

#include "ddls.h"
#include "components/cc1101/cc1101.h"


#define CC1101_CONF_REG_LEN 47
const uint8_t cc1101_default_cfg[] = {
		0x42,	//IOCFG2	0x00
		0x2E,	//IOCFG1	0x01
		0x01,	//IOCFG0	0x02
		0x47,	//FIFOTHR	0x03
		0xD3,	//SYNC1		0x04
		0x91,	//SYNC0		0x05
		0x3D,	//PKTLEN	0x06
		0x0B,	//PKTCTRL1	0x07
		0x05,	//PKTCTRL0	0x08
		0x00,	//ADDR		0x09
		0x00,	//CHANNR	0x0A
		0x06,	//FSCTRL1	0x0B
		0x00,	//FSCTRL0	0x0C
		0x20,	//FREQ2		0x0D
		0x28,	//FREQ1		0x0E
		0xC5,	//FREQ0		0x0F
		0xF5,	//MDMCFG4	0x10
		0x75,	//MDMCFG3	0x11
		0x13,	//MDMCFG2	0x12
		0x22,	//MDMCFG1	0x13
		0xE5,	//MDMCFG0	0x14
		0x46,	//DEVIATN	0x15
		0x07,	//MCSM2		0x16
		0x30,	//MCSM1		0x17
		0x18,	//MCSM0		0x18
		0x1D,	//FOCCFG	0x19
		0x1C,	//BSCFG		0x1A
		0xC7,	//AGCTRL2	0x1B
		0x00,	//AGCTRL1	0x1C
		0xB2,	//AGCTRL0	0x1D
		0x87,	//WOREVT1	0x1E
		0x6B,	//WOREVT0	0x1F
		0xF8,	//WORCTRL	0x20
		0xB6,	//FREND1	0x21
		0x10,	//FREND0	0x22
		0xEA,	//FSCAL3	0x23
		0x2A,	//FSCAL2	0x24
		0x00,	//FSCAL1	0x25
		0x1F,	//FSCAL0	0x26
		0x41,	//RCCTRL1	0x27
		0x00,	//RCCTRL0	0x28
		0x59,	//FSTEST	0x29
		0x7f,	//PTEST		0x2A
		0x3f,	//AGCTEST	0x2B
		0x81,	//TEST2		0x2C
		0x35,	//TEST1		0x2D
		0x09	//TEST0		0x2E
	};

struct Array{
	uint8_t data[64];
	uint8_t length;
} ddls_buff;

uint8_t addr;
DdlsPacket rxPacket;

void ddls_init(){
	
	cc1101_init();
	cc1101_send_strobe(CC1101_STROBE_SRES);
	cc1101_write_burst(0, cc1101_default_cfg, CC1101_CONF_REG_LEN);
	cc1101_send_strobe(CC1101_STROBE_SIDLE);
	uint8_t status = cc1101_send_strobe(cc1101_make_strobe(CC1101_HDR_WRITE, CC1101_STROBE_SNOP));
	ddls_nvm_init();
	rxPacket.len = 0;
	ddls_buff.length = 0;
	
}

void ddls_read_packet(void){

	uint8_t status = cc1101_send_strobe(cc1101_make_strobe(CC1101_HDR_READ, CC1101_STROBE_SNOP));
	uint8_t ddls_buff.length = cc1101_get_fifo_bytes(status);
	cc1101_read_burst(CC1101_REG_FIFO, ddls_buff.data, ddls_buff.length);
	cc1101_read_burst(cc1101_make_header(CC1101_HDR_READ, CC1101_HDR_BURST, CC1101_REG_STS_LQI), &(rxPacket.lqi), 2);
	rxPacket.len = ddls_buff[0];
	rxPacket.addr_to = ddls_buff[1];
	rxPacket.addr_from = ddls_buff[2];
	rxPacket.payload = (ddls_buff+3);
	rxPacket.lqi &= 0x7F;

}

void ddls_send_packet(uint8_t* data, uint8_t len){

	cc1101_write_burst(CC1101_REG_FIFO, data, len);
	cc1101_send_strobe(CC1101_STROBE_STX);

}

void process_ddls(void){
	if(rxPacket.len){

	ddls_buff.length = 0;
	rxPacket.len = 0;
	}
	
}
