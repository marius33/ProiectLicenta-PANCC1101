/*
 * ddls_nvm.h
 *
 * Created: 19/06/2016 20:27:29
 *  Author: Marius
 */ 


#ifndef DDLS_NVM_H_
#define DDLS_NVM_H_

#include <common_nvm.h>

#define FLASHCALW_UNIQUE_ID_ADDR 0x0080020C
#define FLASHCALW_UNIQUE_ID_LEN 15

void ddls_nvm_init(void);

void ddls_nvm_write(void* buff, int32_t len);

void ddls_nvm_read(void* buff, int32_t len);





#endif /* DDLS_NVM_H_ */