


#include <asf.h>
#include <stdbool.h>

#include <core_cm4.h>
void disableSWO(void)
{
	CoreDebug_Type *core = CoreDebug_BASE;
	core->DEMCR = (0<<CoreDebug_DEMCR_TRCENA_Pos);
}

/* USB related */
struct {
	uint8_t expected_length;
	uint8_t data[64];
	uint8_t length;
} usb_buff;

bool authorize_cdc_transfer = false;
void usb_init(void){
	usb_buff.length = 0;
	usb_buff.expected_length = 0;
	udc_start();
}

bool cdc_enable(void){
	authorize_cdc_transfer = true;
	return true;
}

void cdc_disable(void){
	authorize_cdc_transfer = false;
}
void usb_rx_notify(void){

	if(authorize_cdc_transfer){
		while(udi_cdc_is_rx_ready()){
			if(usb_buff.expected_length == 0)	
				usb_buff.expected_length = udi_cdc_getc();
			else		
				usb_buff.data[usb_buff.length++] = udi_cdc_getc();
		}
	}
}

/* writes data in the usb tx buffer while it can 
	returns the numbes of bytes that have not been written yet
	0 - on success
	>0 - on not done
*/
uint8_t usb_write(uint8_t* data, uint8_t len){
	while(udi_cdc_is_tx_ready() && (len--))
			udi_cdc_putc(*(data++));

	return len;
}

/* Blocking function that waits untill a character can be put in the tx usb buffer */
static inline void udi_cdc_wait_tx_ready(void){
	while(!udi_cdc_is_tx_ready());
}

void usb_transmit(uint8_t* data, uint8_t len){
	
	udi_cdc_wait_tx_ready();
	udi_cdc_putc(len);
	while(len--){
		udi_cdc_wait_tx_ready();
		udi_cdc_putc(*(data++));
	}
}

void process_usb(void){
	if(usb_buff.length==usb_buff.expected_length){
	
	/* Do stuff here */

	usb_buff.length = 0;
	usb_buff.expected_length = 0;
	}
	
}

//GDO0 asserts when a packet has been received with CRC OK. De-asserts when the first byte is read from the RX FIFO.
static void eic_callback_gdo0(void)
{
	if (eic_line_interrupt_is_pending(EIC, EXT_INT4)) {
		eic_line_clear_interrupt(EIC, EXT_INT4);
		ddls_read_packet();
	}
}

static void eic_setup(void)
{
	eic_enable(EIC);
	struct eic_line_config eic_line_conf;
	eic_line_conf.eic_mode = EIC_MODE_EDGE_TRIGGERED;
	eic_line_conf.eic_edge = EIC_EDGE_RISING_EDGE;
	eic_line_conf.eic_level = EIC_LEVEL_HIGH_LEVEL;
	eic_line_conf.eic_filter = EIC_FILTER_ENABLED;
	eic_line_conf.eic_async = EIC_ASYNCH_MODE;

	eic_line_set_config(EIC, EXT_INT4, &eic_line_conf);
		
	eic_line_set_callback(EIC, EXT_INT4, eic_callback_gdo0, EIC_4_IRQn, 1);

	eic_line_enable(EIC, EXT_INT4);

}

int main (void){

	disableSWO();
	sysclk_init();	
 	irq_initialize_vectors();
 	cpu_irq_enable();
	board_init();
 	usb_init();	
	eic_setup();
	ddls_init();

	ioport_set_pin_level(PIN_PA11, false);
	ioport_set_pin_level(PIN_PA12, true);

	while(true){
		process_usb();
		process_ddls();

	}

}
