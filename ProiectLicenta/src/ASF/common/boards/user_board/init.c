/**
 * \file
 *
 * \brief User board initialization template
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <ioport.h>
#include <sysclk.h>

static inline void ioport_set_pin_peripheral_mode(int pin, int mode)
{
	ioport_set_pin_mode(pin, mode);
	ioport_disable_pin(pin);
}

void board_init(void)
{
	// basically initializes peripheral/gpio sysclk
	ioport_init();
	
	//initialize spi
	ioport_set_pin_peripheral_mode(PIN_PA21, MUX_PA21A_SPI_MISO);
	ioport_set_pin_peripheral_mode(PIN_PA22, MUX_PA22A_SPI_MOSI);
	ioport_set_pin_peripheral_mode(PIN_PA23, MUX_PA23A_SPI_SCK);
	ioport_set_pin_peripheral_mode(PIN_PA24, MUX_PA24A_SPI_NPCS0);

	//pins for led
	ioport_set_pin_dir(PIN_PA11, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(PIN_PA12, IOPORT_DIR_OUTPUT);

	//pins for cc1101 gpio
	ioport_set_pin_peripheral_mode(PIN_PA19, MUX_PA19C_EIC_EXTINT4);
	ioport_set_pin_dir(PIN_PA20, IOPORT_DIR_INPUT);
		
	#ifdef CONF_BOARD_USB_PORT
	ioport_set_pin_peripheral_mode(PIN_PA25, MUX_PA25A_USBC_DM);
	ioport_set_pin_peripheral_mode(PIN_PA26, MUX_PA26A_USBC_DP);
	#endif
		
}


